# Hydrogen Water Maker

Established in 2006, located in Guangzhou south China. Guangzhou Olans water treatment equiptments Co., Ltd is a professional manufacturer and exporter of High-rich hydrogen water maker,Hydrogen righ water generator,hydrogen water machine,Portable hydrogen drinking water bottle,hydrogen water pitcher,water filter kettleAir purifiers, Water purifiers, Water dispensers and related accessories. We own a powerful R&D team with highly qualified designers and engineers , which annually launch many new models. we provide our customers with OEM and ODM services.

Olans has achieved ISO9001:2008 Quality System Certification, the Chinese National Mandatory Product 3C Certification and the National Drinking Water Product Health Safety Certification by the Ministry of Public Health of China.

With excellent quality and sincere services, our sales volume doubles every year which enables us to develop core technology,expand our factory and purchase top class production and inspection&test equipment, and now we are one of the leading manufacturer with 11 R&D staffs, 16 sales, 50 inspectors, and more than 200 operators in 20000 square meters factory.

All for Customers, Quality First, Guide by Market,Continuous Innovation, Convenience and Utility, Reasonable Price are our philosophy. Believe we are your best choice for we will provide excellent quality and perfect service, also backed up by our powerful R&D team and ample experience with customers, we Olans People with great passion and confidence are striding forward with you to make mutual benefits and create bright future.

# Why Choose Olansi Hydrogen Water Maker

1. Professional Factory
Established in 2006, located in Guangzhou, China, professional hydrogen water maker machine manufacturer with FDA Certification,with our own strong R&D Team and Laboratory..

2. Excellent Factory Management
Stable employee, most of them are 8 years long in this factory. High output with monthly capacity 50000pcs. 98% in time delivery to customers.

3. Professional in Hydrogen Water Maker Quality
Olansi always take the concept: All for Customers, Quality First, Guide by Market, Continuous innovation and persist in All Function, Convenience and Utility, and with Reasonable Price.

Website :  http://www.hydrogenwatermaker.net